const axios = require('axios').default;

const getLugarLatLong =  async ( address) => {

    const encodedUrl = encodeURI( address );


    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${ encodedUrl }`,
        timeout: 1000,
        headers: {'x-rapidapi-key' : '738eb3f40amsh0fefc830fb4b369p150bb8jsn85b325e4d1e1'}
    });


    const resp = await instance.get();

    if (resp.data.Results.length === 0 ){
        throw new Error(`No hay resultados para ${ address }`);
    }

    const data = resp.data.Results[0];
    const direccion = data.name;
    const lat = data.lat;
    const lng = data.lng;

    return {
        direccion,
        lat,
        lng
    }

}


module.exports = {
    getLugarLatLong
}

